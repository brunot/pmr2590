package com.example.webcamtoandroid;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class WebcamActivity extends Activity {

	EditText textOut;
	TextView textIn;
	ImageView imageView;
	GetImageTask task;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webcam);
		Log.d("Started", "Hello");
		// new GetImageTask().execute("");
		textIn = (TextView) findViewById(R.id.textin);
		textOut = (EditText) findViewById(R.id.textout);
		imageView = (ImageView) findViewById(R.id.imageView1);
		Button buttonSend = (Button) findViewById(R.id.send);
		buttonSend.setOnClickListener(buttonSendOnClickListener);
		Button buttonStop = (Button) findViewById(R.id.stop);
		buttonStop.setOnClickListener(buttonStopOnClickListener);
	}

	class GetImageTask extends AsyncTask<String, String, String> {
		Socket socket = null;
		DataOutputStream dataOutputStream = null;
		DataInputStream dataInputStream = null;
		boolean finishedVideo = false;

		protected void onProgressUpdate(String... feed) {

			byte[] decodedString = null;
			try {
				decodedString = Base64.decode(feed[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.d("ErrorHere", "" + e);
			}
			Log.d("St--", ":" + decodedString.length);
			Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
					decodedString.length);

			imageView.setImageBitmap(bitmap);
		}

		@Override
		protected String doInBackground(String... params) {
			finishedVideo = false;
			try {
				if (socket == null) {
					socket = new Socket(params[0], 13267);
				}
				while (!finishedVideo) {
					dataOutputStream = new DataOutputStream(
							socket.getOutputStream());
					dataInputStream = new DataInputStream(
							socket.getInputStream());
					if (dataInputStream != null) {
						// dataOutputStream.writeUTF(textOut.getText().toString());
						String base64Code = dataInputStream.readUTF();

						Log.d("String", ":" + base64Code);
						//
						publishProgress(base64Code);
					} else {
						finishedVideo = true;
					}
				}

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.d("Error", "" + e);
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (dataOutputStream != null) {
					try {
						dataOutputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (dataInputStream != null) {
					try {
						dataInputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			return null;
		}
	}

	Button.OnClickListener buttonSendOnClickListener = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// textIn.setText("Yay2");
			task = new GetImageTask();
			task.execute(textOut.getText().toString());

		}
	};
	
	Button.OnClickListener buttonStopOnClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			task.finishedVideo = true;
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.webcam, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
