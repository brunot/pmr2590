import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvResize;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

import org.apache.commons.codec.binary.Base64;

import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

/**

 */
public class WorkerRunnable implements Runnable {
	private static DataInputStream dataInputStream;
	private static DataOutputStream dataOutputStream;
	protected Socket clientSocket = null;
	protected String serverText = null;

	public WorkerRunnable(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	public void run() {
		try {
			dataInputStream = new DataInputStream(clientSocket.getInputStream());
			dataOutputStream = new DataOutputStream(
					clientSocket.getOutputStream());
			System.out.println("Accepted connection : " + clientSocket);

			FrameGrabber grabber = new OpenCVFrameGrabber("");
			grabber.start();
			// File myFile = new
			// File("C:\\Users\\Bruno\\workspace\\WebcamToAndroid\\smiley.png");
			System.out.println("Sending...");
			IplImage frame = grabber.grab();
			IplImage resizeImage;
			// grabber.grab();
			// FileInputStream imageInFile = new FileInputStream();
			// byte imageData[] = new byte[(int) myFile.length()];
			// imageInFile.read(imageData);

			/*
			 * Converting Image byte array into Base64 String
			 */
			String imageDataString;
			while (frame != null) {
					resizeImage = IplImage.create(120, 120, frame.depth(),
							frame.nChannels());
					cvResize(frame, resizeImage);
					ByteBuffer bb = frame.asByteBuffer();
					byte[] b = new byte[bb.remaining()];
					cvSaveImage("frame.jpg", resizeImage);
					bb.get(b);
					File myFile = new File("frame.jpg");
					FileInputStream imageInFile = new FileInputStream(myFile);
					byte imageData[] = new byte[(int) myFile.length()];
					imageInFile.read(imageData);
					imageDataString = encodeImage(imageData);
					System.out.println(imageDataString.length());
					dataOutputStream.writeUTF(imageDataString);

					dataOutputStream.flush();
				try{
					Thread.sleep(60);
					frame = grabber.grab();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				clientSocket.close();
				}
	}catch (IOException e) {
		} catch (Exception e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	}

	public static String encodeImage(byte[] imageByteArray) {
		return Base64.encodeBase64String(imageByteArray);
	}
}